<?php
include 'scripts/db.php';
include 'scripts/polymorhism.php';
$db = new Dbquerys();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $sku = $_POST['sku'];
    $name = $_POST['name'];
    $price = $_POST['price'];
    $type_id = $_POST['type_id'];
    $product = $db->createProduct($sku, $name, $price, $type_id);
    $table_name = $db->findTypeNameById($type_id);
    $type = new $table_name;
    $type->set($type);

    header('Location:index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script type="text/javascript" src="scripts/main.js"></script>
    <title>Project page</title>
</head>
<div class="ms-auto navbar bg-dark navbar-dark fixed-top">
    <h2 class="ps-5" style="color:white">Product Add</h2>
    <div class="float-end pe-5">
        <button type="submit" form="product_form" class="btn btn-outline-success me-5" style="color:white">Save</button>
        <a href="index.php" class="btn btn-outline-danger me-5" style="color:white">Cancel</a>
    </div>
</div>

<body style="padding-top:70px; padding-left:20px;">
    <form id="product_form" method="post">
        <div class="row mb-3 w-25">
            <label class="col-sm-2 col-form-label">SKU</label>
            <div class="col-sm-10">
                <input name="sku" class="form-control" id="sku" type="text" required>
            </div>
        </div>
        <div class="row mb-3 w-25">
            <label class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
                <input name="name" class="form-control" id="name" type="text" required>
            </div>
        </div>
        <div class="row mb-3 w-25">
            <label class="col-sm-2 col-form-label">Price ($)</label>
            <div class="col-sm-10">
                <input name="price" oninvalid="this.setCustomValidity('Please, submit required data')" oninput="setCustomValidity('')" class="form-control" id="price" type="number" step=any min=0 required>
            </div>
        </div>
        <div class="select">
            <label>Type Switcher</label>
            <select name="type_id" onchange="javascript:create_div(this.value);" id="productType" required>
                <option value="" selected>Select a type</option>
                <?php foreach ($db->getAllFromTable('types') as $type) : ?>
                    <option value="<?= $type['id'] ?>"><?= $type['type'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div id="div_main" style="padding-top:20px;"></div>
    </form>
</body>

</html>