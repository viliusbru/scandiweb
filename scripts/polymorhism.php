<?php

abstract class BaseProduct
{
    abstract public function get($type, $sku);
    abstract public function set();
}

class Furniture extends BaseProduct
{
    public function get($type, $sku)
    {
        $db = new Dbquerys;
        $data = $db->findTypeDataBySku($type, $sku);
        return  'Dimensions: ' . $data['height'] . 'x' . $data['width'] . 'x' . $data['lenght'];
    }
    public function set()
    {
        $db = new Dbquerys();
        $sku = $_POST['sku'];
        $height = $_POST['height'];
        $width = $_POST['width'];
        $lenght = $_POST['lenght'];
        $db->createFurnitureTypeData($height, $width, $lenght, $sku);
    }
}
class Book extends BaseProduct
{
    public function get($type, $sku)
    {
        $db = new Dbquerys;
        $data = $db->findTypeDataBySku($type, $sku);
        return 'Weight: ' . $data['weight'] . 'KG';
    }
    public function set()
    {
        $db = new Dbquerys();
        $sku = $_POST['sku'];
        $weight = $_POST['weight'];
        $db->createBookTypeData($weight, $sku);
    }
}
class Dvd extends BaseProduct
{
    public function get($type, $sku)
    {
        $db = new Dbquerys;
        $data = $db->findTypeDataBySku($type, $sku);
        return 'Size: ' . $data['size'] . ' MB';
    }
    public function set()
    {
        $db = new Dbquerys();
        $sku = $_POST['sku'];
        $size = $_POST['size'];
        $db->createDvdTypeData($size, $sku);
    }
}
