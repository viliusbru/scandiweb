function create_div(num) {
  var html = "";
  if (num == "3") {
    var html = `
        <div>Please, provide size
            <div class="row mb-3 w-25">
                <label class="col-sm-2 col-form-label">Size (MB)</label>
                    <div class="col-sm-10">
                        <input name="size" class="form-control" id="size" type="text" required>
                    </div>
            </div>
            </div>`;
  } else if (num == "2") {
    var html = `
        <div>Please, provide weight
            <div class="row mb-3 w-25">
                <label class="col-sm-2 col-form-label">Weight (KG)</label>
                    <div class="col-sm-10">
                        <input name="weight" class="form-control" id="weight" type="number" required>
                    </div>
            </div>
            </div>`;
  } else if (num == "1") {
    var html = `
        <div>Please, provide dimensions
            <div class="row mb-3 w-25">
                <label class="col-sm-2 col-form-label">Height (CM)</label>
                    <div class="col-sm-10">
                        <input name="height" class="form-control" id="height" type="text" required>
                    </div>
            </div>
            <div class="row mb-3 w-25">
                <label class="col-sm-2 col-form-label">Width (CM)</label>
                    <div class="col-sm-10">
                        <input name="width" class="form-control" id="width" type="text" required>
                    </div>
            </div>
            <div class="row mb-3 w-25">
                <label class="col-sm-2 col-form-label">Length (CM)</label>
                    <div class="col-sm-10">
                        <input name="lenght" class="form-control" id="lenght" type="text" required>
                    </div>
            </div>
            </div
            `;
  }
  document.getElementById("div_main").innerHTML = html;
}
