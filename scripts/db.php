<?php

class Dbcon
{

    public function connect()
    {
        $conn = new PDO("mysql:host=localhost;dbname=products_task", 'admin', 'admin');

        return $conn;
    }
}
class Dbquerys extends Dbcon
{
    // GETs

    public function getAllFromTable($table_name)
    {
        $sql = "SELECT * FROM $table_name";
        $result = $this->connect()->query($sql);

        return $result;
    }
    public function getAllFromTableWithType()
    {
        $sql = "SELECT * FROM products LEFT JOIN types ON products.type_id = types.id;";
        $result = $this->connect()->query($sql);

        return $result;
    }
    public function getTypeNameByProductId($product_id)
    {
        $sql = 'SELECT type FROM types INNER JOIN products ON products.type_id = types.id WHERE products.id = :product_id;';
        $result = $this->connect()->prepare($sql);
        $result->execute([':product_id' => $product_id]);
        $data = $result->fetch(PDO::FETCH_ASSOC);

        return $data;
    }
    public function findTypeNameById($id)
    {
        $sql = "SELECT * FROM types WHERE id = :id";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([':id' => $id]);
        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        return $data['type'];
    }
    public function findTypeDataBySku($table_name, $sku)
    {
        $sql = "SELECT * FROM $table_name WHERE product_sku = :sku";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([':sku' => $sku]);
        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        return $data;
    }

    // CREATEs
    public function createProduct($sku, $name, $price, $type_id)
    {
        $sql = "INSERT INTO products(sku, name, price, type_id) VALUES (?, ?, ?, ?)";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$sku, $name, $price, $type_id]);
    }
    public function createDvdTypeData($data, $sku)
    {
        $sql = "INSERT INTO dvd(size, product_sku) VALUES (?, ?)";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$data, $sku]);
    }
    public function createBookTypeData($data, $sku)
    {
        $sql = "INSERT INTO book(weight, product_sku) VALUES (?, ?)";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$data, $sku]);
    }
    public function createFurnitureTypeData($height, $width, $lenght, $sku)
    {
        $sql = "INSERT INTO furniture(height, width, lenght, product_sku) VALUES (?, ?, ?, ?)";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$height, $width, $lenght, $sku]);
    }
    public function createTypeData($table_name, $data, $sku)
    {
        $string = "INSERT INTO " . $table_name . '(';
        $string .= implode(", ", array_keys($data)) . ', product_sku) VALUES (';
        $string .= "'" . implode(',', array_fill(0, count($data) + 1, '?')) . "')";
        $stmt = $this->connect()->prepare($string);
        echo array_values($data);
        $stmt->execute([implode(',', array_values($data)), $sku]);
    }

    // DELETEs
    public function deleteProduct($id)
    {
        $sql = "DELETE FROM products WHERE FIND_IN_SET(id, :ids)";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([':ids' => implode(',', $id)]);
    }
}
