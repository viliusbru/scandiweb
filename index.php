<?php
include 'scripts/db.php';
include 'scripts/polymorhism.php';
$db = new Dbquerys();
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $todelete = $_POST['checkbox'];
    if (!empty($todelete)) {
        $db->deleteProduct($todelete);
    }
    header('Location:index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Project page</title>
</head>
<div class="ms-auto navbar bg-dark navbar-dark fixed-top">
    <h2 class="ps-5" style="color:white">Product List</h2>
    <div class="float-end pe-5">
        <a href="add.php" class="btn btn-outline-success me-5" style="color:white">ADD</a>
        <button type="submit" id="delete-product-btn" form="delete" class="btn btn-outline-danger me-5" style="color:white">MASS DELETE</button>
    </div>
</div>

<body style="padding-top:70px; padding-left:20px" ;>
    <form id="delete" method="POST">
        <div class="product-class row">
            <?php foreach ($db->getAllFromTableWithType() as $product) : ?>
                <div class="card text-center col-sm-3 p-3 m-1" style="width: 18rem;">
                    <input name="checkbox[]" value="<?= $product['id'] ?>" id="<?= $product['id'] ?>" type="checkbox" class="delete-checkbox"></dev>
                    <div class="card-body">
                        <h5 class="card-title"><?= $product['sku'] ?></h5>
                        <h5 class="card-title"><?= $product['name'] ?></h5>
                        <h5 class="card-title"><?= $product['price'] . '$' ?></h5>
                        <h5 class="card-title">
                            <?php
                            $product_type = $product['type'];
                            $data = new $product_type;
                            echo $data->get($product_type, $product['sku']);
                            ?>
                        </h5>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </form>
</body>

</html>